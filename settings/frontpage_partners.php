<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Version details
 *
 * @package   theme_adaptable
 * @copyright 2017 Yevhen Matasar <matasar.ei@gmail.com>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 *
 */

// Frontpage / project partners.
$temp = new admin_settingpage('theme_adaptable_frontpage_partners', get_string('frontpagepartnerssettings', 'theme_adaptable'));

$temp->add(new admin_setting_heading('theme_adaptable_partners', get_string('partnersshowsettingsheading', 'theme_adaptable'),
            format_text(get_string('partnersdesc', 'theme_adaptable'), FORMAT_MARKDOWN)));


$name = 'theme_adaptable/partnersenabled';
$title = get_string('partnersenaled', 'theme_adaptable');
$description = get_string('partnersenableddescr', 'theme_adaptable');
$setting = new admin_setting_configcheckbox($name, $title, $description, 0);
$temp->add($setting);

// Number of partners.
$name = 'theme_adaptable/partnerscount';
$title = get_string('partnerscount', 'theme_adaptable');
$description = get_string('partnerscountdesc', 'theme_adaptable');
$default = THEME_ADAPTABLE_DEFAULT_SLIDERCOUNT;
$setting = new admin_setting_configselect($name, $title, $description, $default, $choices1to12);
$setting->set_updatedcallback('theme_reset_all_caches');
$temp->add($setting);

// If we don't have an slide yet, default to the preset.
$partnerscount = get_config('theme_adaptable', 'partnerscount');

if (!$partnerscount) {
    $partnerscount = THEME_ADAPTABLE_DEFAULT_PARTNERSCOUNT;
}

for ($partnersindex = 1; $partnersindex <= $partnerscount; $partnersindex++) {
    $pfield = 'partner' . $partnersindex;
    $name = 'theme_adaptable/' . $pfield;
    $title = get_string('partnerimage', 'theme_adaptable');
    $description = get_string('partnerimagedesc', 'theme_adaptable');
    $setting = new admin_setting_configstoredfile($name, $title, $description, $pfield);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $name = "theme_adaptable/{$pfield}url";
    $title = get_string('partnerurl', 'theme_adaptable');
    $description = get_string('partnerurldesc', 'theme_adaptable');
    $setting = new admin_setting_configtext($name, $title, $description, '', PARAM_URL);
    $temp->add($setting);

    $name = "theme_adaptable/{$pfield}cap";
    $title = get_string('partnercaption', 'theme_adaptable');
    $description = get_string('partnercaptiondesc', 'theme_adaptable');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $temp->add($setting);
}

$ADMIN->add('theme_adaptable', $temp);